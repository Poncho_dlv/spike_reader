#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xml.etree.ElementTree as ElementTree
from replay_type.BoardActionType import BoardActionType
from replay_type.RollType import RollType


class ReplayReader:

    def __init__(self, file_name):
        self.__file_name = file_name
        self.match_data = {}
        self.players_data = {}
        self.team_data = {}
        self.home_team_roster = []
        self.away_team_roster = []

        # context variable
        self.current_action = None
        self.current_action_effect = None

    def parse(self):
        tree = ElementTree.parse(self.__file_name)
        root = tree.getroot()

        for child in root:
            for sub_child in child:
                if sub_child.tag == "GameInfos":
                    self.read_game_info(sub_child)
                elif sub_child.tag == "BoardState":
                    self.read_board_state(sub_child)
                elif sub_child.tag == "RulesEventBoardAction":
                    self.read_rules_event_board_action(sub_child)
                elif sub_child.tag == "RulesEventSpecialAction":
                    self.read_rules_event_special_action(sub_child)
                #elif sub_child.tag == "RulesEventAddMercenary":
                    #self.read_rules_event_add_mercenary(sub_child)
        return self.match_data, self.players_data

    def read_game_info(self, elem):
        for child in elem:
            if child.tag == "BallType":
                self.match_data["ball_type"] = child.text
            elif child.tag == "CoachesInfos":
                if not self.match_data.get("coaches"):
                    self.match_data["coaches"] = [{}, {}]
                    self.match_data["coaches"][0]["name"] = child[0][1].text
                    self.match_data["coaches"][0]["team"] = "home"
                    self.match_data["coaches"][1]["name"] = child[1][1].text
                    self.match_data["coaches"][1]["team"] = "away"
            elif child.tag == "RowLeague":
                for sub_child in child:
                    if sub_child.tag == "Id":
                        league_id = sub_child[0].text
                        league_id = league_id.replace("RowId:", "").replace("-Server:ONLINE-StoreName:Main", "")
                        self.match_data["league_id"] = league_id
                    if sub_child.tag == "Name":
                        self.match_data["league_name"] = sub_child.text
                    if sub_child.tag == "Websites":
                        self.match_data["league_website"] = sub_child.text
            elif child.tag == "RowCompetition":
                for sub_child in child:
                    if sub_child.tag == "Id":
                        competition_id = sub_child[0].text
                        competition_id = competition_id.replace("RowId:", "").replace("-Server:ONLINE-StoreName:Main", "")
                        self.match_data["competition_id"] = competition_id
                    if sub_child.tag == "Name":
                        self.match_data["competition_name"] = sub_child.text

    def read_board_state(self, elem):
        weather = elem.find("Meteo")
        if weather is not None:
            self.match_data["weather"] = weather.text

        team_list = elem.find('ListTeams')
        if team_list is not None:
            team_list = team_list.findall("TeamState")
            if team_list and len(team_list) == 2:
                self.read_team_state(team_list[0], "home")
                self.read_team_state(team_list[1], "away")

    def read_team_state(self, elem, team: str = "home"):
        for player_state in elem.find("ListPitchPlayers"):
            internal_id = player_state.find("Id").text
            if not self.players_data.get(internal_id):
                data = player_state.find("Data")
                if data.find("LobbyId") is not None:  # In case of Mercenary this elem doesnt exist we create the structure
                    player_id = data.find("LobbyId").text.replace("RowId:", "").replace("-Server:ONLINE-StoreName:Main", "")
                    player_name = data.find("Name").text
                    self.players_data[internal_id] = {
                        "team": team,
                        "id": int(player_id),
                        "name": player_name,
                        "sustained_foul": 0,
                        "sustained_stuns_foul": 0,
                        "sustained_ko_foul": 0,
                        "sustained_injuries_foul": 0,
                        "sustained_dead_foul": 0,
                        "inflicted_foul": 0,
                        "inflicted_stuns_foul": 0,
                        "inflicted_ko_foul": 0,
                        "inflicted_injuries_foul": 0,
                        "inflicted_dead_foul": 0,
                        "sent_off": False,
                        "mercenary": False,
                        "injury_roll_modifier": 0
                    }
                else:
                    # Mercenary
                    player_name = data.find("Name").text
                    self.players_data[internal_id] = {
                        "team": team,
                        "name": player_name,
                        "sustained_foul": 0,
                        "sustained_stuns_foul": 0,
                        "sustained_ko_foul": 0,
                        "sustained_injuries_foul": 0,
                        "sustained_dead_foul": 0,
                        "inflicted_foul": 0,
                        "inflicted_stuns_foul": 0,
                        "inflicted_ko_foul": 0,
                        "inflicted_injuries_foul": 0,
                        "inflicted_dead_foul": 0,
                        "sent_off": False,
                        "mercenary": True,
                        "injury_roll_modifier": 0
                    }

    def read_rules_event_board_action(self, elem):
        internal_id = elem.find("PlayerId")
        if internal_id is not None:
            internal_id = elem.find("PlayerId").text
        action_type = elem.find("ActionType")
        if action_type is not None:
            action_type = int(action_type.text)
            if action_type == BoardActionType.FOUL:
                # fouled player
                self.current_action = BoardActionType.FOUL
                if self.players_data.get(internal_id):
                    self.players_data[internal_id]["sustained_foul"] += 1
                    results = elem.find("Results")
                    if results is not None:
                        for board_action_result in results:
                            roll_type = board_action_result.find("RollType")
                            if roll_type is not None:
                                if int(roll_type.text) == RollType.INJURY_ROLL:
                                    coach_choice = board_action_result.find("CoachChoices")
                                    if coach_choice is not None:
                                        injury_roll = coach_choice.find("ListDices")
                                        if injury_roll is not None:
                                            injury_roll = injury_roll.text.replace("(", "").replace(")", "")
                                            injury_roll = injury_roll.split(",")
                                            injury_roll = int(injury_roll[0]) + int(injury_roll[1])
                                            # TODO Manage DP, niggle
                                            if injury_roll < 8:
                                                self.players_data[internal_id]["sustained_stuns_foul"] += 1
                                                self.current_action_effect = "inflicted_stuns_foul"
                                            elif injury_roll == 8 or injury_roll == 9:
                                                # TODO Manage thick skull
                                                self.players_data[internal_id]["sustained_ko_foul"] += 1
                                                self.current_action_effect = "inflicted_ko_foul"
                                            else:
                                                self.players_data[internal_id]["sustained_injuries_foul"] += 1
                                                self.current_action_effect = "inflicted_injuries_foul"
            elif action_type == BoardActionType.REFEREE:
                if self.current_action == BoardActionType.FOUL:
                    # fouler player
                    self.players_data[internal_id]["inflicted_foul"] += 1
                    if self.current_action_effect is not None:
                        self.players_data[internal_id][self.current_action_effect] += 1
                        self.current_action_effect = None
                results = elem.find("Results")
                for board_action_result in results:
                    roll_type = board_action_result.find("RollType")
                    if roll_type is not None:
                        if int(roll_type.text) == RollType.REFEREE_ROLL:
                            coach_choice = board_action_result.find("CoachChoices")
                            if coach_choice is not None:
                                injury_roll = coach_choice.find("ListDices")
                                if injury_roll is not None:
                                    injury_roll = injury_roll.text.replace("(", "").replace(")", "")
                                    injury_roll = injury_roll.split(",")
                                    if injury_roll[0] == injury_roll[1]:
                                        self.players_data[internal_id]["sent_off"] = True

    def read_rules_event_special_action(self, elem):
        action_type = elem.find("ActionType")
        if action_type is not None:
            action_type = int(action_type.text)
            if action_type == BoardActionType.FOUL:
                self.current_action = None

    def read_rules_event_add_mercenary(self, elem):
        mercenary_type = elem.find("MercenaryType")
        if mercenary_type is not None:
            mercenary_type = int(mercenary_type)


    # <RulesEventAddMercenary>
    #       <MercenaryId>15</MercenaryId>
    #       <MercenaryType>304</MercenaryType>
    #       <InducementCategory>7</InducementCategory>
    #       <InducementsCash>150000</InducementsCash>
    #       <Treasury>40000</Treasury>
    #     </RulesEventAddMercenary>

    # Linked MercenaryType with IdPlayerTypes in InducementsCategory
    # Loop
    # <RulesEventInducementsInfos>
    #       <TeamInducements>
    #         <TeamInducements>
    #           <InducementsCash>180000</InducementsCash>
    #           <InducementsCategories>

    # <InducementsCategory>
    #               <Max>1</Max>
    #               <Cost>30000</Cost>
    #               <Type>7</Type>
    #               <Players>
    #                 <PlayerData>
    #                   <Ma>4</Ma>
    #                   <Name>PLAYER_NAMES_CHAMPION_FUNGUS_FALLBACK</Name>
    #                   <Ag>3</Ag>
    #                   <Contract>3</Contract>
    #                   <Value>80</Value>
    #                   <Av>7</Av>
    #                   <St>7</St>
    #                   <ListSkills>(44,76,13,46,54,59)</ListSkills>
    #                   <IdPlayerTypes>304</IdPlayerTypes>
    #                 </PlayerData>
    #               </Players>
    #             </InducementsCategory>