#!/usr/bin/env python
# -*- coding: utf-8 -*-


class InducementCategoryType:
    IRRELEVANT_MINUS6 = -6  # Don't know what this is either, but seems like it is a GUI thing as well. Leaning
    # towards ignoring the negative values, really
    IRRELEVANT_MINUS5 = -5  # Don't know what this is either, but seems like it is a GUI thing as well. Leaning
    # towards ignoring the negative values, really
    MERCENARY_SAVE = -2  # Not entirely sure what this is, but seems like it's a GUI thing. Used for Bomber
    # Dribblesnot in S28 finals, and he was never on the field
    BLOODWEISER_BABE = 1  # 0 - 2
    BRIBE = 2  # 0 - 3
    EXTRA_TEAM_TRAINING = 3  # 0 - 4
    HALFLING_MASTER_CHEF = 4  # 0 - 1
    WANDERING_APOTHECARY = 5  # 0 - 2
    MERCENARY = 6  # Unlimited
    STAR_PLAYER = 7  # 0 - 2
    WIZARD = 8  # 0 - 1
    IGOR = 9
    JOURNEYMAN = 10
