#!/usr/bin/env python
# -*- coding: utf-8 -*-
from enum import IntEnum


class BoardActionType(IntEnum):
        MOVE = 0
        BLOCK = 1
        BLITZ = 2
        PASS = 3
        HAND_OFF = 4
        FOUL = 5
        KNOCK_DOWN = 6
        KICK_OFF = 7
        BOUNCE = 8
        CATCH = 9
        TOUCH_DOWN = 10
        STUNNED_TO_PRONE = 11
        KO_RECOVERY = 12
        PICK_UP_BALL = 14
        NEGA_TRAIT = 15
        RIGHT_STUFF_LANDING = 16
        ALWAYS_HUNGRY_SQUIRM_FREE = 17
        SHADOWING = 18
        STAB = 19
        LEAP = 21
        BALL_AND_CHAIN = 23
        CHAINSAW = 25
        MULTIPLE_BLOCK = 26
        HYPNOTIC_GAZE = 27
        PASS_BLOCK = 29
        HALFLING_MASTER_CHEF = 30
        FIREBALL = 31
        FIREBALL_HIT = 32
        LIGHTNING_BOLT = 33
        REFEREE = 34
        RIGHT_STUFF_LANDING_ON_ANOTHER_PLAYER = 35
        MOVE_PLAYER_UNDER_BALL = 37
        DODGE_FROM_DIVING_TACKLE = 39
        MULTIPLE_BLOCK_STAB = 41
        ACTIVATE_PLAYER = 42
        NUMBER_OF_FANS = 46
        WEATHER = 47
        SWELTERING_HEAT = 48
        FEED = 49
        BOMB_DIRECT_HIT = 50
        BOMB_ADJACENT_HIT = 51
        THROW_A_BOMB = 52
        BOMB_CATCH = 53
        BOMB_SCATTER = 54
        BOMB_RETHROW = 55
