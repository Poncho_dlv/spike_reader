#!/usr/bin/env python
# -*- coding: utf-8 -*-
from enum import IntEnum


class RollType(IntEnum):
    ARMOR_ROLL = 3
    INJURY_ROLL = 4
    REFEREE_ROLL = 15
