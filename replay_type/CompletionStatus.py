#!/usr/bin/env python
# -*- coding: utf-8 -*-
from enum import IntEnum


class KickOffTableType(IntEnum):
    Completed = 0
    Conceded = 1
    Disconnected = 2
    AdminResult = 8825
