#!/usr/bin/env python
# -*- coding: utf-8 -*-
from enum import IntEnum


class KickOffTableType(IntEnum):
    UNKNOWN = 0
    GET_THE_REF = 2
    RIOT = 3
    PERFECT_DEFENCE = 4
    HIGH_KICK = 5
    CHEERING_FANS = 6
    CHANGING_WEATHER = 7
    BRILLIANT_COACHING = 8
    QUICK_SNAP = 9
    BLITZ = 10
    THROW_A_ROCK = 11
    PITCH_INVASION = 12
