#!/usr/bin/python

import getopt
import json
import sys

from ReplayReader import ReplayReader


def main(argv):
    input_file = ""
    output_file = ""
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('run.py -i <input_xml> -o <output_json_file>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('run.py -i <input_xml> -o <output_json_file>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_file = arg
        elif opt in ("-o", "--ofile"):
            output_file = arg

    reader = ReplayReader(input_file)
    match_data, player_data = reader.parse()
    output_json = {
        "match_data": match_data,
        "player_data": player_data
    }

    with open(output_file, 'w') as outfile:
        json.dump(output_json, outfile)
    outfile.close()
    sys.exit(0)


if __name__ == "__main__":
    main(sys.argv[1:])
